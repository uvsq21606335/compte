package comptebancaire;

/**
 * Compte: Classe representant un compte bancaire avec des methodes de debit, credit et virement
 * @Author Mohamed Ramdane DEBIANE
 * @Version 0.1
 *
 */


public class compte {
    private int solde;

    compte(int initial) throws Initialisationnegative{
        if(initial < 0) throw new Initialisationnegative();
        this.solde = initial;
    }

    /**
     * Retourne le solde du compte courant sous la forme d'un entier
     * @return solde du compte
     */
    public int getSolde(){
        return this.solde;
    }

    /**
     * Méthode permettant de virer de l'argent sur un compte
     * @param credit Montant de la somme à crediter sur le compte
     * @throws Initialisationnegative
     */
    public void crediter(int credit) throws Initialisationnegative {
        if(credit < 0) throw new Initialisationnegative();
        this.solde = this.getSolde() + credit;
    }

    /**
     * Methode permettant de debitter de l'argent d'un compte qui n'est pas à decouvert
     * @param debit Montant de la somme à debitter
     * @throws Initialisationnegative
     */
    public void debiter(int debit) throws Initialisationnegative{
        if(debit < 0) throw new Initialisationnegative();
        if(this.getSolde() - debit > 0) this.solde = this.getSolde() - debit;
    }

    /**
     * Methode permettant de virer de l'argent vers un autre compte
     * @param somme Somme devant sortir du compte créditeur
     * @param recv Compte qui doit recevoir la somme
     * @throws Initialisationnegative
     */
    public void virement(int somme, compte recv) throws Initialisationnegative{
        if(somme < 0) throw new Initialisationnegative();
        this.debiter(somme);
        recv.crediter(somme);
    }
}

