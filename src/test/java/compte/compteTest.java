package comptebancaire;

import org.junit.* ;
import static org.junit.Assert.* ;

public class compteTest {
    private int initial = 200;
    private int initialneg = -200;
    private int vir = 50;


    @Test
    public void testinitialisation() throws Initialisationnegative{
       compte c = new compte(initial);
       assertEquals(initial, c.getSolde());
    }
    @Test(expected = Initialisationnegative.class)
    public void testinitialisationneg() throws Initialisationnegative{
            compte c = new compte(initialneg);
            assertEquals(initial, c.getSolde());
    }
    @Test
    public void testCredit() throws Initialisationnegative {
        compte c = new compte(initial);
        c.crediter(10);
        assertEquals(initial + 10, c.getSolde());
    }
    @Test
    public void testDebit() throws Initialisationnegative {
        compte c = new compte(initial);
        c.debiter(10);
        assertEquals(initial -10, c.getSolde());
    }
    @Test
    public void testVirement() throws Initialisationnegative{
        compte c1 = new compte(initial);
        compte c2 = new compte(initial);

        c1.virement(vir, c2);

        assertEquals(initial -vir, c1.getSolde());
        assertEquals(initial + vir, c2.getSolde());

    }
}